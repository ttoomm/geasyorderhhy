package biz.foodservice.tools;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/**
 * Created by hx on 4/22/15.
 */
public class NetworkTools {



    static public String getContent(URL url) throws IOException {
        String s = "";
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        byte[] buf = new byte[2000];
        for (int i = in.read(buf); i > 0; i = in.read(buf)) {
            s += new String(buf, 0, i);
        }
        in.close();
        return s;
    }

    static public void loadPicToLocal(URL url, String filename) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        OutputStream os = new BufferedOutputStream(new FileOutputStream(filename));
        byte[] buf = new byte[20000];
        for (int i = in.read(buf); i > 0; i = in.read(buf)) {
            os.write(buf, 0, i);
        }
        os.close();
        in.close();
    }



    final static String tag=NetworkTools.class.getCanonicalName();


    public static String postJsonData(String url, Object o) throws IOException {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        ObjectMapper om = new ObjectMapper();
        String jsMsg = om.writeValueAsString(o);
        StringEntity se = new StringEntity(jsMsg);
        se.setContentType("application/json;charset=UTF-8");
        se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
        post.setEntity(se);
        HttpResponse resp = httpclient.execute(post);

        HttpEntity resultentity = resp.getEntity();
        InputStream inputstream = resultentity.getContent();
        Header contentencoding = resp.getFirstHeader("Content-Encoding");
        if (contentencoding != null && contentencoding.getValue().equalsIgnoreCase("gzip")) {
            inputstream = new GZIPInputStream(inputstream);
        }

        String resultstring = convertStreamToString(inputstream);
        inputstream.close();
        return resultstring;
    }

    static private String convertStreamToString(InputStream is) throws IOException {
        String line = "";
        StringBuilder total = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        while ((line = rd.readLine()) != null) {
            total.append(line);
        }

        return total.toString();
    }




}
