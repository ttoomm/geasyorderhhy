package biz.foodservice.tools;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by hx on 4/28/15.
 */
public class CommonTool {
    public static void showExceptionMsg(Context ctx, String tag, String head, Exception e) {
        head += String.format(", [%s - %s]: %s", e.getClass().getCanonicalName(), e.getCause(), e.getMessage());
        Toast.makeText(ctx, head, Toast.LENGTH_LONG).show();
        Log.e(tag, head, e);
    }
    public static void showMsg(Context ctx, String tag, String head) {
        Toast.makeText(ctx, head, Toast.LENGTH_LONG).show();
        Log.d(tag, head);
    }
}
