package biz.foodservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import zm.waiter.DeliverTaskModel;


public class Order {

    final public static String TYPE = Order.class.getCanonicalName();

    public static enum OrderType {
        EATIN, TAKEOUT, DELIVERY;//
    }

    /*
     * data model
     * */
    public boolean handled;
    public boolean paid;

    public boolean delivered;

    public List<Long> cookingTasks;
    /**
     * table ID
     */
    public long table;
    /**
     * orderID
     */
    public long id;
    public long timestamp;

    /**
     * dishID - dish number
     */
    public Map<Long, Long> dishes;

    @JsonIgnore
    public long customer;

    public Order() {
        this.dishes = new HashMap<Long, Long>();
        this.cookingTasks=new ArrayList<Long>();
    }

    public Order(long table, long customer) {
        this();
        this.table = table;
        this.customer = customer;
    }


    public void addDish(long dish, long num) {
        this.dishes.put(dish, num);
    }


    /*



        PayOrderView view;
        public PayOrderView getView(Context ctx){
            if(this.view!=null) {
                Log.d("aa",LOG_ARROW+"task table "+view.orderModel.table +"  get old view");
                return this.view;
            }
            else{
                this.view= new PayOrderView(ctx, this);
                Log.d("aa",LOG_ARROW+"task table "+view.orderModel.table+ " get new view");
                return this.view;
            }
        }
        */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        Order other = (Order) o;
        if (this.id == other.id)
            return true;
        return super.equals(o);
    }

}
