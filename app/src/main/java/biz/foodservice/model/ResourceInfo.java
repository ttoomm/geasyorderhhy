package biz.foodservice.model;

import android.util.Log;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import biz.foodservice.tools.NetworkTools;

public class ResourceInfo {


    final static private String tag=ResourceInfo.class.getCanonicalName();

    final static private String RestResourcePath = "/rest/resource";
    final static private String RestCatagoryPath = RestResourcePath+"/catagory";
    final static private String RestDishPath = RestResourcePath+"/dish";
    final static private String localStorageDishesObj="dishes.obj";
    final static private String localStorageCatagoriesObj="catagories.obj";
    final static private String localStorageDishMedia="dishImage";

    public final String localStorageFileDirPath, localCatagoriesFile, localDishesInfoFile, localDishMediaDir;



    List<Catagory> catagories;
    List<Dish> dishes;


    public ResourceInfo(String localStoragedir){
        this.localStorageFileDirPath = localStoragedir;
        this.localCatagoriesFile = localStorageFileDirPath +"/"+localStorageDishesObj;
        this.localDishesInfoFile = localStorageFileDirPath +"/"+localStorageCatagoriesObj;
        this.localDishMediaDir= localStorageFileDirPath +"/"+localStorageDishMedia;
        new File(localDishMediaDir, "icons").mkdirs();
        new File(this.localDishMediaDir, "images").mkdirs();

    }

    public boolean isLocalResourceExist() {
        File d = new File(this.localCatagoriesFile);
        File c = new File(this.localDishesInfoFile);
        return d.exists() && c.exists();
    }

    public List<Dish> getDishesInfo() {
        return this.dishes;
    }


    public List<Catagory> getCatagoryInfo() {
        return this.catagories;
    }

    static private URL getCatagoryUrl(String host, int port) throws MalformedURLException {
        String s = String.format("http://%s:%d%s", host, port, RestCatagoryPath);
        return new URL(s);
    }

    static private URL getDishInfoUrl(String host, int port) throws MalformedURLException {
        String s = String.format("http://%s:%d%s", host, port, RestDishPath);
        return new URL(s);
    }

    static private URL getDishUrl(String host, int port, long id) throws MalformedURLException {
        String s = String.format("http://%s:%d%s/%d", host, port, RestDishPath, id);
        return new URL(s);
    }

    private String getDishIconFilename(long id) {
        return String.format("%s/icons/dishIcon_%d.jpg", this.localDishMediaDir, id);
    }

    private String getDishPicFilename(long id) {

        return String.format("%s/images/dishPic_%d.jpg", this.localDishMediaDir, id);
    }

    public static class _DishInfo{
        public long id=0;
        public String name="";
        public long price=0;
    }

    public static class _Dish  {
        public static class _Blob{
            public byte[] bytes=new byte[1];
        }
        public _DishInfo info = new _DishInfo();
        public String desc = "";
        public String iconUrl = "";
        public String picUrl = "";
        public _Blob pic =new _Blob();
        public _Blob icon = new _Blob();
    }

    public void loadResourceFromServer(String host, int port) throws IOException {

        URL urlCatagories=getCatagoryUrl(host, port);
        Log.d(tag, "start downloading the catagories information from server");
        String jsonCatagory= NetworkTools.getContent(urlCatagories);

        URL urlDishes=getDishInfoUrl(host, port);
        Log.d(tag, "start downloading the dishes information from server");
        String jsonDishes=NetworkTools.getContent(urlDishes);

        ObjectMapper om=new ObjectMapper();
        this.catagories=om.readValue(new StringReader(jsonCatagory), new TypeReference<ArrayList<Catagory>>(){});
        List<_DishInfo> _dishesBasic=om.readValue(new StringReader(jsonDishes), new TypeReference<ArrayList<_DishInfo>>(){});

        this.dishes = new ArrayList<Dish>();
        for(_DishInfo _info: _dishesBasic){
            String picFilename=getDishPicFilename(_info.id);
            String iconFilename=getDishIconFilename(_info.id);
            URL dishUrl=getDishUrl(host, port, _info.id);
            Log.d(tag, "start downloading the dish detail from server, dish id: "+_info.id);
            String jsonDish= NetworkTools.getContent(dishUrl);
            _Dish _dish=om.readValue(new StringReader(jsonDish), new TypeReference<_Dish>() {});


            OutputStream os = new BufferedOutputStream(new FileOutputStream(iconFilename));
            os.write(_dish.icon.bytes);
            os.close();
            os = new BufferedOutputStream(new FileOutputStream(picFilename));
            os.write(_dish.pic.bytes);
            os.close();

            Dish dish=new Dish(_dish.info.id, _dish.info.name, _dish.desc, iconFilename, picFilename, _dish.info.price);
            this.dishes.add(dish);
        }
        this.storeToLocal();
    }


    /**
     * **init the catagories***
     */

    public void loadFromLocal() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.localDishesInfoFile ));
        this.dishes = (List<Dish>) ois.readObject();
        ois.close();

        ois = new ObjectInputStream(new FileInputStream(this.localCatagoriesFile));
        this.catagories = (List<Catagory>) ois.readObject();
        ois.close();
    }

    public void storeToLocal() throws IOException {

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.localCatagoriesFile));
        oos.writeObject(this.getCatagoryInfo());
        oos.close();

        oos = new ObjectOutputStream(new FileOutputStream(this.localDishesInfoFile));
        oos.writeObject(this.getDishesInfo());
        oos.close();
    }

}
