package biz.foodservice.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Catagory implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5336063561102024937L;

    public int id = 0;
    public String name = "";
    public String desc = "";
    public String pic = "";
    public List<Long> dishes;
    public String type;

    public String rev;

    public Catagory() {
        this.dishes = new ArrayList<Long>();
        this.type = this.getClass().getCanonicalName();
    }

    public Catagory(int id, String name, String desc, String pic, List<Long> dishes) {
        this();
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.pic = pic;
        this.dishes = dishes;
    }

    public Catagory(JSONObject json) throws JSONException {


        this();


        this.id = json.getInt("id");
        this.name = json.getString("name");
        this.desc = json.getString("desc");

        JSONArray ja = json.getJSONArray("dishes");
        for (int i = 0; i < ja.length(); i++)
            this.dishes.add(ja.getLong(i));


    }


    @Override
    public String toString() {
        return this.name;
    }

}
