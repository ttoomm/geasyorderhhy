package biz.foodservice.model;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 */
public class Dish implements Serializable {
    static public enum Nutrition {
        ServingSize("ServingSize"), Calories("Calories"), CaloriesFromFat("CaloriesFromFat"),
        TotalFat("TotalFat", 65), SaturatedFat("SaturatedFat", 20), TransFat("TransFat"), Cholesterol("Cholesterol", 300),
        Sodium("Sodium", 2400), TotalCarb("TotalCarb"), DietaryFiber("DietaryFiber"),
        Sugars("Sugars"), Protein("Protein");


        final public float recommended;
        String name;

        Nutrition(String name, float recommanded) {
            this.recommended = recommanded;
            this.name = name;
        }

        Nutrition(String name) {
            this.recommended = -1;
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }


    }

    static public enum Allergen {
        Wheat, Soy, Peanuts, Treenuts, Fish, Shellﬁsh, Eggs, Milk
    }



    private static final long serialVersionUID = -2232451143034469173L;


    public long id = 0;
    public String name = "";
    public long price = 0;
    public String desc = "";

    public String icon = "";
    public String pic = "";

    public Map<Nutrition, Float> nutritions;
    public Map<Allergen, Boolean> allergens;


    public String type;


    public Dish() {
        this.type = this.getClass().getCanonicalName();
    }


    public Dish(long id, String name, String desc, String icon, String pic, long price) {
        this();
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.icon = icon;
        this.pic = pic;
    }

    private Map<Nutrition, Float> initNutritionsFromJson(JSONObject jo) {
        Map<Nutrition, Float> m = new HashMap<Nutrition, Float>();
/*
        for(Object k: jo.keySet()){
			String s=(String) k;
			Nutrition n=Nutrition.valueOf(s);
			double d=11;//jo.getDouble(s);
			m.put(n, (float)d);
		}
		*/
        return m;
    }

    private Map<Allergen, Boolean> initAllergensFromJson(JSONObject jo) {
        Map<Allergen, Boolean> m = new HashMap<Allergen, Boolean>();
        /*
        for(Object k: jo.keySet()){
			String s=(String) k;
			Allergen a=Allergen.valueOf(s);
			boolean b=false;//jo.getBoolean(s);
			m.put(a, b);
		}
		*/
        return m;
    }



    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Dish)) return false;
        if (this == o) return true;
        Dish i = (Dish) o;
        return this.id == i.id;
    }

    @Override
    public int hashCode() {
        return new Long(this.id).hashCode();
    }
//class Info


    public static Dish getDishModel(List<Dish> dishes, long id) {
        for (Dish inf : dishes) {
            if (inf.id == id)
                return inf;
        }
        return null;
    }


}
