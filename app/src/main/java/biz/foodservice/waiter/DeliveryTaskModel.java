package biz.foodservice.waiter;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import biz.foodservice.model.Dish;
import biz.foodservice.tools.NetworkTools;


public class DeliveryTaskModel implements Comparable<DeliveryTaskModel> {
    public static final String tag=DeliveryTaskModel.class.getCanonicalName();

    public static long status_ready = -1, status_onDelivery = 0,
            status_finished = 1, status_HandleByPayment = 2;

    public long id = -1;
    public long byWaiter = -1;
    public long tableId = -1;

    public long dishId = -1;

    public Date orderDate = new Date(Long.MAX_VALUE);

    public long status = -1;
    public long orderId=-1;

    public long cookingTaskId = -1;

    @JsonIgnore
    List<Dish> dishInfoModels;

    @JsonIgnore
    DeliveryTaskView view;

    @JsonIgnore
    DeliveryTaskView iconview;

    public DeliveryTaskModel(){
    }

    public DeliveryTaskModel(List<Dish> dishInfoModels) {
       this();
       this.dishInfoModels=dishInfoModels;
    }





    static private String path_claim_task="/rest/transaction/delivery/claim";
    static private String path_finish_task="/rest/transaction/delivery/finish";

    private void setStatus(long status) {

        this.status=status;
        //set view
        if(this.view!=null){
            this.view.setStatus(status);
        }


    }

    @JsonIgnore
    private void setStatus(long status, String host, int port) throws Exception {
        this.setStatus(status);
        String restPath="";
        if(status== DeliveryTaskModel.status_onDelivery){
            restPath=path_claim_task;
        }else if(status== DeliveryTaskModel.status_finished){
            restPath=path_finish_task;
        }
        String url=String.format("http://%s:%d%s", host, port, restPath);
        String result=NetworkTools.postJsonData(url, this);
        if(result==null || !result.equals(""+true)) throw new Exception("the result from server is not true, but "+result);
    }

    //put the task from waiting to executing

    @JsonIgnore
	public void starting(long waiterId, String host, int port) throws Exception {

		if(this.status!= DeliveryTaskModel.status_ready){
            String msg="internal error or database is not consistent,  start a task, the task is not in waiting status, but -  " + this.status + ", the dish id - " + this.dishId;
            throw new Exception(msg);
		}
		this.byWaiter=waiterId;
		this.setStatus(DeliveryTaskModel.status_onDelivery, host, port);
	}

    @JsonIgnore
	public void finish(String host, int port) throws Exception {
		if(this.status!= DeliveryTaskModel.status_onDelivery){
			Log.e(tag, "something wrong: start a task, the task is not in waiting status, but -  "+this.status+", the dish id - "+this.dishId);
			return;
		}

         this.setStatus(DeliveryTaskModel.status_finished, host, port);

    }

    @JsonIgnore
    public DeliveryTaskView getView(Context ctx) {
        if (this.view != null)
            return this.view;
        else {
            this.view = new DeliveryTaskView(ctx, this, true, dishInfoModels);
            view.setStatus(status);//
            return this.view;
        }
    }

    @JsonIgnore
    public DeliveryTaskView getIconView(Context ctx) {
        if (this.iconview != null)
            return this.iconview;
        else {
            this.iconview = new DeliveryTaskView(ctx, this, true, dishInfoModels);
            return this.iconview;
        }
    }



    final static String path_getUnfinishedTask="/rest/transaction/delivery/unfinished";
    final static String path_getWaitingTask="/rest/transaction/delivery/getWaiting";

	public static List<DeliveryTaskModel> getAllUnfinishedTask(String  host, int port) throws IOException {

        String json=NetworkTools.getContent(new URL(String.format("http://%s:%d%s", host, port, path_getUnfinishedTask)));
        Log.d(tag, "tasks json message got from server: ");
        Log.d(tag, json);
        ObjectMapper om=new ObjectMapper();
		List<DeliveryTaskModel> tasks=null;
        tasks = om.readValue(new StringReader(json), new TypeReference<ArrayList<DeliveryTaskModel>>() { });
		return tasks;
	}
	public static List<DeliveryTaskModel> getWaitingTask(String host, int port) throws IOException {
        String json=NetworkTools.getContent(new URL(String.format("http://%s:%d%s", host, path_getWaitingTask)));
        ObjectMapper om=new ObjectMapper();
        List<DeliveryTaskModel> tasks=new ArrayList<DeliveryTaskModel>();
        try {

            tasks = om.readValue(new StringReader(json), new TypeReference<List<DeliveryTaskModel>>() { });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tasks;
	}






    @Override
    public int compareTo(DeliveryTaskModel to) {
        if(this.status!=to.status){
            return this.status>to.status?1: -1;
        }
        if(this.orderDate!=to.orderDate) {
            return this.orderDate.before(this.orderDate)? 1: -1;
        }
        return this.tableId>this.tableId? 1:-1;
    }

}