package biz.foodservice.waiter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.Gravity;
import android.widget.CheckedTextView;

import java.util.List;

import biz.foodservice.R;
import biz.foodservice.model.Dish;

public class DeliveryTaskView extends CheckedTextView {
    final BitmapFactory.Options options;
    final static String tag= DeliveryTaskView.class.getCanonicalName();

    DeliveryTaskModel tm;

    public DeliveryTaskView(Context context, DeliveryTaskModel tm, List<Dish> dishInfoModels) {

        this(context, tm, false, dishInfoModels);


    }

    List<Dish> dishInfoModels;

    public DeliveryTaskView(Context context, DeliveryTaskModel tm, boolean small, List<Dish> dishInfoModels) {

        super(context);
        this.options = new BitmapFactory.Options();
        this.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);

        this.dishInfoModels=dishInfoModels;
        if (small) {
            options.inSampleSize = 2;
            this.setTextColor(Color.CYAN);
        } else {
            options.inSampleSize = 1;
            this.setTextColor(Color.GREEN);
        }


        this.tm = tm;
        for (Dish dm : this.dishInfoModels) {

            if (tm.dishId == dm.id) {

                //this.setText(dm.name+"--["+this.tm.getTotal()+"]");
                this.setText(dm.name);


                Bitmap bm = BitmapFactory.decodeFile(dm.icon, this.options);
                BitmapDrawable sd = new BitmapDrawable(bm);
                Drawable badge = this.writeOnDrawable(R.drawable.badge, "" + this.tm.tableId);
                LayerDrawable ld = new LayerDrawable(new Drawable[]{sd, badge});
                this.setBackground(ld);
                this.setStatus(this.tm.status);
                //this.setCompoundDrawablesWithIntrinsicBounds(null,  sd,null,   null);
            }
        }

    }

    public BitmapDrawable writeOnDrawable(int drawableId, String text) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.YELLOW);
        paint.setTextSize(25);
        paint.setStrokeWidth(1.1f);

        Canvas canvas = new Canvas(bm);
        Log.d(tag, String.format("w: %d, h: %d", bm.getWidth(), bm.getHeight()));
        //canvas.drawText(text, bm.getWidth()-15-text.length()*4, 20, paint);
        canvas.drawText("Table: "+text, bm.getWidth() * (float) (0.9 - 0.4), bm.getHeight() * (float) (0.1 + 0.03), paint);


        return new BitmapDrawable(bm);
    }

    public void setStatus(long status) {


        if (status == DeliveryTaskModel.status_onDelivery) {
            Drawable sd = this.getResources().getDrawable(R.drawable.food_delivery);


            Drawable a = this.getBackground();
            Rect rect = a.getBounds();
            sd.setBounds(rect);


            LayerDrawable ld = new LayerDrawable(new Drawable[]{a, sd});
            ld.setBounds(rect);

//			this.setCompoundDrawablesWithIntrinsicBounds(null,  ld,null,   null);
            this.setBackground(ld);

        }

    }


    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        this.setCheckMarkDrawable(isChecked() ? R.drawable.icon_choice : R.drawable.gv_selected_no);
    }

}
