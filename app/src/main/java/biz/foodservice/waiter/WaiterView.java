package biz.foodservice.waiter;


import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

public class WaiterView extends TextView {
    public WaiterModel cm;

    public WaiterView(Context context, WaiterModel cm) {
        super(context);
        this.cm = cm;
        this.setWidth(100);

        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);

        this.setGravity(Gravity.CENTER_VERTICAL);
        this.setLayoutParams(lp);

        this.setText(this.cm.name);

    }

}
