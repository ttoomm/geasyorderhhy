package biz.foodservice.waiter;

public class WaiterModel {
    public String name;
    public long id;

    public WaiterModel(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
