package biz.foodservice.waiter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;

import biz.foodservice.R;
import biz.foodservice.model.Dish;

public class OngoingTaskView extends LinearLayout {
    static private final String tag=OngoingTaskView.class.getCanonicalName();
    WaiterModel chief;
    DeliveryTaskModel task;
    Context ctx;

    LinearLayout ll;

    List<Dish> dishInfoModels;


    public OngoingTaskView(Context context, WaiterModel chief, DeliveryTaskModel task, List<Dish> dishInfoModels) {
        super(context);
        this.ctx = context;
        this.task = task;
        this.dishInfoModels=dishInfoModels;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.task_view, this, true);


        this.setOrientation(LinearLayout.HORIZONTAL);
        this.setGravity(Gravity.CENTER_VERTICAL);
        this.setMinimumHeight(75);


        ll = (LinearLayout) this.getChildAt(0);


        this.chief = chief;
        WaiterView vChief = new WaiterView(context, chief);

        ll.addView(vChief);


    }

    public long getChiefId() {
        return chief.id;
    }


    //	TaskView vTask;
    void setTask(DeliveryTaskModel t) {
        if(this.task!=null)
            ll.removeView(this.task.getIconView(ctx));
        if(t==null){
            this.task = null;
            return;
        }
        this.task = t;
        View v = this.task.getIconView(ctx);

        ll.addView(v, 150, 75);
    }




}
