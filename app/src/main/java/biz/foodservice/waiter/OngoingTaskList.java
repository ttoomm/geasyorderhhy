package biz.foodservice.waiter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import biz.foodservice.model.Dish;

public class OngoingTaskList extends BaseAdapter {


//	List<ChiefView> vChiefs;

    List<OngoingTaskView> vTasks;

    List<Dish> dishInfoModels;




    public OngoingTaskList(Context ctx, List<WaiterModel> mChiefs, List<Dish> dishInfoModels) {


        this.vTasks = new ArrayList<OngoingTaskView>();
        this.dishInfoModels=dishInfoModels;
        for (WaiterModel chief : mChiefs) {
            this.vTasks.add(new OngoingTaskView(ctx, chief, null, this.dishInfoModels));
        }
    }

    public void assignTask(long idChief, DeliveryTaskModel t){
        for(OngoingTaskView v: vTasks){
            if(v.getChiefId()==idChief){
                v.setTask(t);
            }
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.vTasks.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        Log.d("aa", "OngoingTaskList--the size of array: " + this.vTasks.size());
        return this.vTasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return this.vTasks.get(position).chief.id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return this.vTasks.get(position);

    }


}
