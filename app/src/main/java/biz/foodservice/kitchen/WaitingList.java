package biz.foodservice.kitchen;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import biz.foodservice.model.Dish;


public class WaitingList extends BaseAdapter {
    public static final String tag = WaitingList.class.getCanonicalName();



    List<CookingTaskModel> cookingTasks;
    List<Dish> dishInfos;
    Context ctx;

    //List<TaskView> taskViews;


    public WaitingList(List<Dish> dishInfos, Context ctx) {
        this.cookingTasks = new ArrayList<CookingTaskModel>();
        this.dishInfos = dishInfos;
        this.ctx = ctx;

        Log.d(tag, "waitlist init");
    }

    @Override
    public int getCount() {

        int i = this.cookingTasks.size();
        Log.d(tag,  "total items: " + i);
        return i;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return this.cookingTasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return this.cookingTasks.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        long idDish = this.cookingTasks.get(position).dish;

        Log.d(tag,  "get meta data for dish, id: " + idDish);

        for (Dish info : this.dishInfos) {
            if (info.id == idDish) {
                View v= this.cookingTasks.get(position).getView(ctx);
                v.setScaleY(0.7f);
                return v;
            }
        }
        return null;

    }





	
	public void pullTasks(String host, int port) throws IOException {

		this.cookingTasks= CookingTaskModel.getAllUnfinishedTask(host, port);
        for(CookingTaskModel t: this.cookingTasks){
            t.dishInfoModels=this.dishInfos;
        }

		Collections.sort(this.cookingTasks);
		
	}


    public List<CookingTaskModel> getTaskList() {
        synchronized (this.cookingTasks) {
            return this.cookingTasks;
        }
    }


}
