package biz.foodservice.kitchen;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import biz.foodservice.R;
import biz.foodservice.main.Configuration;
import biz.foodservice.model.Dish;
import biz.foodservice.model.ResourceInfo;
import biz.foodservice.tools.CommonTool;


public class KitchenView extends Activity implements OnClickListener, OnItemClickListener {


    TextView tvMsg;

    static void log(String msg) {
        Log.d(tag, msg);
    }

    static final String tag = KitchenView.class.getCanonicalName();

    String host;
    int port;


    List<Dish> dishInfoModels;


    /**
     * get chief info
     */
    List<ChiefModel> chiefs;

    List<ChiefModel> getChiefs() {
        List<ChiefModel> chiefs = new ArrayList<ChiefModel>();
        chiefs.add(new ChiefModel(1, "John"));
        chiefs.add(new ChiefModel(2, "Tom"));
        return chiefs;
    }


    public KitchenView() throws FileNotFoundException, IOException, ClassNotFoundException {
        super();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kitchen);

        SharedPreferences settings = this.getSharedPreferences(Configuration.preferenceName, 0);
        this.host = settings.getString("server", "");
        this.port = settings.getInt("port", -1);

        if (this.host.length() < 1 || this.port < 0) {
            Toast.makeText(this, "the server setting is not valid, set up the server at the first page", Toast.LENGTH_LONG);
            Intent intent = new Intent(this, Configuration.class);
            this.startActivity(intent);
        }

        setContentView(R.layout.kitchen);
        ResourceInfo ri = new ResourceInfo(this.getFilesDir().getAbsolutePath());
        try {
            ri.loadFromLocal();
        } catch (IOException e) {
            String msg = "fail to load the data from local storage";
            CommonTool.showExceptionMsg(this, tag, msg, e);
        } catch (ClassNotFoundException e) {
            String msg = "fail to load the data from local storage";
            CommonTool.showExceptionMsg(this, tag, msg, e);
        }
        this.dishInfoModels = ri.getDishesInfo();


        this.chiefs = this.getChiefs();

        this.tvMsg = (TextView) this.findViewById(R.id.txtOpInfo);
        this.tvMsg.setText("");


        waitingTaskGrid = (GridView) this.findViewById(R.id.waitingList);
        waitingTaskGrid.setHorizontalScrollBarEnabled(true);
        waitingTaskGrid.setChoiceMode(waitingTaskGrid.CHOICE_MODE_SINGLE);



        wl = new WaitingList(this.dishInfoModels, this);


        waitingTaskGrid.setAdapter(wl);


        this.waitingTaskGrid.setOnItemClickListener(this);


        ongoingTaskList = (ListView) this.findViewById(R.id.Chiefs);
        this.ongoings = new OngoingTaskList(this, this.chiefs, this.dishInfoModels);

        ongoingTaskList.setAdapter(this.ongoings);
        ongoingTaskList.setOnItemClickListener(this);


        btnStartPreparing = (Button) this.findViewById(R.id.btnStartPrepare);
        btnStartPreparing.setOnClickListener(this);
        this.btnStartPreparing.setEnabled(false);
        btnFinishPrepare = (Button) this.findViewById(R.id.btnFinish);
        btnFinishPrepare.setOnClickListener(this);
        this.btnFinishPrepare.setEnabled(false);
        this.btnRetrieveTasks = (Button) this.findViewById(R.id.btnRetrieveTasks);
        this.btnRetrieveTasks.setOnClickListener(this);


        //this.test();

        this.refreshTaskList();

        log("oncreate done");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chief, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    Button btnStartPreparing;
    Button btnFinishPrepare;
    Button btnRetrieveTasks;
    GridView waitingTaskGrid;
    WaitingList wl;
    OngoingTaskList ongoings;
    ListView ongoingTaskList;


    //selected task
    CookingTaskModel selectedTask;

//		//selected Chief
//		ChiefModel selectedChief;

    OngoingTaskView selectedOngoing;


    @Override
    public void onClick(View v) {


        if (v == this.btnFinishPrepare) {
            if (this.selectedOngoing.task == null) return;
            try {
                this.selectedOngoing.task.finish(this.host, this.port);

            } catch (Exception e) {
                CommonTool.showExceptionMsg(this, tag, "fail to submit the finished task to server", e);
            }
            this.refreshTaskList();

        } else if (v == this.btnStartPreparing) {
            if (this.selectedTask == null || this.selectedOngoing == null || this.selectedOngoing.task != null)
                return;

            try {
                this.selectedTask.starting(this.selectedOngoing.getChiefId(), this.host, this.port);
            } catch (Exception e) {
                CommonTool.showExceptionMsg(this, tag, "fail to submit the finished task to server", e);
            }

            this.refreshTaskList();
        } else if (v == this.btnRetrieveTasks) {
            this.refreshTaskList();
        }
    }


    public void refreshTaskList() {


        Log.i(tag, "start pulling task from server");
        try {
            this.wl.pullTasks(this.host, this.port);

        } catch (IOException e) {
            CommonTool.showExceptionMsg(this, tag, "error during pulling the tasks", e);
        }
        this.wl.notifyDataSetChanged();

        //Todo, logic problem, if the task is gone, the ongoing task won't have chance to remove its current view.
        for (OngoingTaskView tt : this.ongoings.vTasks) {
            tt.setTask(null);
        for (CookingTaskModel t : this.wl.cookingTasks) {
            if (t.status != CookingTaskModel.PROCESSING) continue;

                if (t.byChief == tt.getChiefId())
                    this.ongoings.assignTask(t.byChief, t);
            }
        }
        this.ongoings.notifyDataSetChanged();

    }

    static private Dish getDishById(long dishId, List<Dish> dishes) {
        for (Dish dish : dishes) {
            if (dish.id == dishId) {
                return dish;
            }
        }
        return null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        if (((Object) this.waitingTaskGrid) == (Object) parent) {

            this.selectedTask = (CookingTaskModel) this.wl.getItem(position);

            //if the task is in cooking ....
            if (this.selectedTask.status != CookingTaskModel.WAITING) {

                String dishName = getDishById(this.selectedTask.dish, this.dishInfoModels).name;
                this.tvMsg.setText(String.format("%s is in processing", dishName));
                this.selectedTask = null;
                this.btnStartPreparing.setEnabled(false);
                this.btnFinishPrepare.setEnabled(false);
                return;
            }
            /*
            if the chief is selected, then enable the start preparing function(enable the button)
             */
            else if (this.selectedOngoing != null) {
                if (this.selectedOngoing.task == null) {
                    this.btnStartPreparing.setEnabled(true);
                    this.btnFinishPrepare.setEnabled(false);
                    String dishName = getDishById(this.selectedTask.dish, this.dishInfoModels).name;
                    this.tvMsg.setText(String.format("%s, you r going to preparing %s", this.selectedOngoing.chief.name, dishName));
                    return;
                } else {
                    this.btnFinishPrepare.setEnabled(true);
                    String dishName = getDishById(this.selectedOngoing.task.dish, this.dishInfoModels).name;
                    this.tvMsg.setText(String.format("%s, you are about to finish %s", this.selectedOngoing.chief.name, dishName));
                }
            }


        }
        if (((Object) this.ongoingTaskList) == (Object) parent) {
            this.selectedOngoing = (OngoingTaskView) this.ongoings.getItem(position);

            //the chief has already cooking
            if (this.selectedOngoing.task != null) {
                String dishName = getDishById(this.selectedOngoing.task.dish, this.dishInfoModels).name;
                this.tvMsg.setText(String.format("%s, you are about to finish %s", this.selectedOngoing.chief.name, dishName));
                this.btnFinishPrepare.setEnabled(true);
                this.btnStartPreparing.setEnabled(false);
                return;

            } else {
                if (this.selectedTask != null && this.selectedTask.status!= CookingTaskModel.PROCESSING) {
                    this.btnStartPreparing.setEnabled(true);
                    String dishName = getDishById(this.selectedTask.dish, this.dishInfoModels).name;
                    this.tvMsg.setText(String.format("%s is about to prepare %s", this.selectedOngoing.chief.name, dishName));
                } else {
                    this.btnStartPreparing.setEnabled(false);
                    this.tvMsg.setText(String.format("%s, please select your task.", this.selectedOngoing.chief.name));
                }
                this.btnFinishPrepare.setEnabled(false);
                return;
            }
        }


    }

}
