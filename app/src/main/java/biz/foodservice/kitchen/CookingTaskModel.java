package biz.foodservice.kitchen;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.foodservice.model.Dish;
import biz.foodservice.tools.NetworkTools;


public class CookingTaskModel implements Comparable<CookingTaskModel> {
    public static final String tag=CookingTaskModel.class.getCanonicalName();

    static public final long FINISHED=1, WAITING=-1, PROCESSING=0;

    public long  status;
    /**
     * Task ID
     */
    public long id=-1;

    @JsonProperty("dishId")
    public long dish=-1;



    /**
     * orderID--dish number
     */
    public Map<Long, Long> quantity;
    public Date dateEarliest = new Date(Long.MAX_VALUE);
    public long byChief = -1;

    @JsonIgnore
    List<Dish> dishInfoModels;

    @JsonIgnore
    CookingTaskView view;

    @JsonIgnore
    CookingTaskView iconview;

    public CookingTaskModel(){
        this.quantity=new HashMap<Long, Long>();
        this.dishInfoModels=new ArrayList<Dish>();
    }

    public CookingTaskModel(int dish, List<Dish> dishInfoModels) {
       this();
        this.quantity = new HashMap<Long, Long>();
        this.dish = dish;
        this.status = CookingTaskModel.WAITING;
        this.dishInfoModels=dishInfoModels;
    }



    @JsonProperty("total")
    public long getTotal(){
        long s=0;
        for(long i: this.quantity.values()){
            s+=i;
        }
        return s;
    }

    @JsonProperty("total")
    public void setTotal(long i){

    }

    static private String path_claim_task="/rest/transaction/task/claim";
    static private String path_finish_task="/rest/transaction/task/finish";

    private void setStatus(long status) {

        this.status=status;
        //set view
        if(this.view!=null){
            this.view.setStatus(status);
        }


    }

    @JsonIgnore
    private void setStatus(long status, String host, int port) throws Exception {
        this.setStatus(status);
        String restPath="";
        if(status== CookingTaskModel.PROCESSING){
            restPath=path_claim_task;
        }else if(status== CookingTaskModel.FINISHED){
            restPath=path_finish_task;
        }
        String url=String.format("http://%s:%d%s", host, port, restPath);
        String result=NetworkTools.postJsonData(url, this);
        if(result==null || !result.equals(""+true)) throw new Exception("the result from server is not true, but "+result);
    }

    //put the task from waiting to executing

    @JsonIgnore
	public void starting(long chief, String host, int port) throws Exception {

		if(this.status!= CookingTaskModel.WAITING){
            String msg="internal error or database is not consistent,  start a task, the task is not in waiting status, but -  " + this.status + ", the dish id - " + this.dish;
            throw new Exception(msg);
		}
		this.byChief=chief;
		this.setStatus(CookingTaskModel.PROCESSING, host, port);
	}

    @JsonIgnore
	public void finish(String host, int port) throws Exception {
		if(this.status!= CookingTaskModel.PROCESSING){
			Log.e(tag, "something wrong: start a task, the task is not in waiting status, but -  "+this.status+", the dish id - "+this.dish);
			return;
		}

         this.setStatus(CookingTaskModel.FINISHED, host, port);

    }

    @JsonIgnore
    public CookingTaskView getView(Context ctx) {
        if (this.view != null)
            return this.view;
        else {
            this.view = new CookingTaskView(ctx, this, true, dishInfoModels);
            view.setStatus(status);//
            return this.view;
        }
    }

    @JsonIgnore
    public CookingTaskView getIconView(Context ctx) {
        if (this.iconview != null)
            return this.iconview;
        else {
            this.iconview = new CookingTaskView(ctx, this, true, dishInfoModels);
            return this.iconview;
        }
    }



    final static String path_getUnfinishedTask="/rest/transaction/task/unfinished";
    final static String path_getWaitingTask="/rest/transaction/task/getWaiting";
    //final static String path_getUnfinishedTask="/rest/transaction/task/";
	public static List<CookingTaskModel> getAllUnfinishedTask(String  host, int port) throws IOException {

        String json=NetworkTools.getContent(new URL(String.format("http://%s:%d%s", host, port, path_getUnfinishedTask)));
        Log.d(tag, "tasks json message got from server: ");
        Log.d(tag, json);
        ObjectMapper om=new ObjectMapper();
		List<CookingTaskModel> tasks=null;
        tasks = om.readValue(new StringReader(json), new TypeReference<ArrayList<CookingTaskModel>>() { });
		return tasks;
	}
	public static List<CookingTaskModel> getWaitingTask(String host, int port) throws IOException {
        String json=NetworkTools.getContent(new URL(String.format("http://%s:%d%s", host, path_getWaitingTask)));
        ObjectMapper om=new ObjectMapper();
        List<CookingTaskModel> tasks=new ArrayList<CookingTaskModel>();
        try {
            tasks = om.readValue(new StringReader(json), new TypeReference<List<CookingTaskModel>>() { });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tasks;
	}






    @Override
    public int compareTo(CookingTaskModel to) {
        if(this.status!=to.status){
            return this.status>to.status?1: -1;
        }
        if(this.dateEarliest!=to.dateEarliest) {
            return this.dateEarliest.before(to.dateEarliest)? -1: 1;
        }
        return this.dish<this.dish? -1:1;
    }

}