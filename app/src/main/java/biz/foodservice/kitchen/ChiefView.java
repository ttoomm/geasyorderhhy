package biz.foodservice.kitchen;


import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

public class ChiefView extends TextView {
    public ChiefModel cm;

    public ChiefView(Context context, ChiefModel cm) {
        super(context);
        this.cm = cm;
        this.setWidth(100);

        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);

        this.setGravity(Gravity.CENTER_VERTICAL);
        this.setLayoutParams(lp);

        this.setText(this.cm.name);

    }

}
