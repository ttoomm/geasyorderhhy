package biz.foodservice.main;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import biz.foodservice.R;
import biz.foodservice.customer.CustomerView;
import biz.foodservice.kitchen.KitchenView;
import biz.foodservice.model.ResourceInfo;
import biz.foodservice.waiter.FoodDeliveryView;

public class Configuration extends Activity implements OnClickListener {
    static final String tag=Configuration.class.getCanonicalName();

    public static final String preferenceName ="serverinfo";

    SharedPreferences settings;

    AlertDialog configDialog;
    private Button btnSave;
    private Button btnCancel;
    private EditText editHost;
    private EditText editPort;
    private Button btnDownload;
    private CheckBox cbOverwrite;
    private Button startCustomerView, startKitchenView, startWaiterView;




    private String host="";
    private int port=80;

    String localStorageDir;

   private ProgressBar pb;
   private TextView labelProgressbar;

   private ResourceInfo ri;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
		 //load config info from device
        this.settings = this.getSharedPreferences(Configuration.preferenceName, 0);

        if(settings!=null){
            this.host=settings.getString("server", "192.168.0.99");
            this.port= settings.getInt("port", 80);
            SharedPreferences.Editor e=this.settings.edit();
            e.putString("server", this.host);
            e.putInt("port", this.port);
            e.commit();
        }

        localStorageDir=this.getFilesDir().getAbsolutePath();



        this.ri=new ResourceInfo(localStorageDir);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        this.setContentView(R.layout.startactivity);

        this.btnDownload= (Button) this.findViewById(R.id.btn_downloadResource);
        this.cbOverwrite= (CheckBox) this.findViewById(R.id.checkbox_overwrite);
        this.cbOverwrite.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        btnDownload.setEnabled(!this.ri.isLocalResourceExist());

        this.labelProgressbar= (TextView) this.findViewById(R.id.labelProgressbar);
        this.pb= (ProgressBar) this.findViewById(R.id.progressbar_downloading);

        this.startCustomerView= (Button) this.findViewById(R.id.start_customer_btn);
        this.startCustomerView.setOnClickListener(this);
        this.startKitchenView= (Button) this.findViewById(R.id.start_kitchen_btn);
        this.startKitchenView.setOnClickListener(this);
        this.startWaiterView= (Button) this.findViewById(R.id.start_waiter_btn);
        this.startWaiterView.setOnClickListener(this);




        this.validateData();


    }
    void validateData(){
        this.setEnableBtns(false);
        if(this.settings==null)
            return ;
        if(this.host.length()<0 || this.port<0)
            return ;
        if(this.ri==null || !this.ri.isLocalResourceExist()){
            return ;
        }
        this.setEnableBtns(true);
    }



    void setEnableBtns(boolean enabled){
        for(Button btn: new Button[]{this.startCustomerView, this.startKitchenView, this.startWaiterView}){
            btn.setEnabled(enabled);
        }
    }



    /**
     * setting dialog****
     */

    private void openSettingDialog() {
        if(this.configDialog!=null){
            this.configDialog.show();
            return;
        }
        this.configDialog = new AlertDialog.Builder(this).create();
        View dialogView = View.inflate(this, R.layout.settings_view, null);
        this.configDialog.setView(dialogView);
        editHost = (EditText) dialogView.findViewById(R.id.editHost);
        editHost.setText(this.host);
        editPort = (EditText) dialogView.findViewById(R.id.editPort);
        editPort.setText(""+this.port);
        btnSave = (Button) dialogView.findViewById(R.id.btnSave);
        btnCancel = (Button) dialogView.findViewById(R.id.btnCancel);
        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        configDialog.show();
    }


    /**
     * ActionBar**
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            openSettingDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onClick(View view) {
        if(view==this.btnCancel){
            this.configDialog.cancel();
            return;
        }
        if(view==this.btnSave){
            this.host=this.editHost.getText().toString();
            this.port=Integer.parseInt(this.editPort.getText().toString());
            SharedPreferences.Editor editor = this.settings.edit();
            editor.putString("server", this.host);
            editor.putInt("port", this.port);
            editor.commit();
            this.configDialog.cancel();
            return;
        }
        if(view==this.cbOverwrite){
            if(this.cbOverwrite.isChecked()){
                this.btnDownload.setEnabled(true);
            }else{
                this.btnDownload.setEnabled(!this.ri.isLocalResourceExist());
            }
            return;
        }

        if(view==this.btnDownload){
            this.btnDownload.setEnabled(false);
            try {
            //    this.labelProgressbar.setVisibility(View.VISIBLE);
            //this.pb.setVisibility(View.VISIBLE);
                Toast.makeText(this, "start downloading the data", Toast.LENGTH_LONG).show();
                this.ri.loadResourceFromServer(this.host, this.port);
                Toast.makeText(this, "finish downloading the data", Toast.LENGTH_LONG).show();
                this.setEnableBtns(true);

            } catch (IOException e) {
                Log.e(tag, "error: " + e.getMessage());
                Toast.makeText(this, "error: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                this.btnDownload.setEnabled(true);
            }
            return;

        }

        Button[] btns=new Button[]{this.startCustomerView, this.startWaiterView, this.startKitchenView};
        Class[] cs=new Class[]{CustomerView.class, FoodDeliveryView.class, KitchenView.class};
        for(int i=0; i<btns.length; i++){
            if(view==btns[i]) {
                Intent intent=new Intent(this, cs[i]);
                this.startActivity(intent);
                return;
            }
        }

    }

    private class ResourceDownloadTask extends AsyncTask<Long, Long, Long>{

        public ResourceDownloadTask(ResourceInfo res){

        }

        @Override
        protected Long doInBackground(Long... longs) {

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            super.onProgressUpdate(values);
        }
    }
}
