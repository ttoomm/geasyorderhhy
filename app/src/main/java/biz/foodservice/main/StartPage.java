package biz.foodservice.main;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import biz.foodservice.R;

public class StartPage extends Activity implements OnPageChangeListener {

    static final String tag=StartPage.class.getCanonicalName();
    //for debug

    private ViewPager mViewPager;

    LinearLayout ll;
    List<pointView> pageIndexImgs;
    int pagecount = 1;
    Button btnstart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mainboard_startpage);

        ActionBar actionBar = getActionBar();
        actionBar.hide();
        btnstart = (Button) findViewById(R.id.btnstart);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setOnPageChangeListener(this);

        MyPageAdapter mPagerAdapter = new MyPageAdapter(this);
        mViewPager.setAdapter(mPagerAdapter);
        this.pagecount = mPagerAdapter.getCount();


        ll = (LinearLayout) this.findViewById(R.id.point);


        this.pageIndexImgs = new ArrayList<pointView>();
        for (int i = 0; i < pagecount; i++) {
//        	PageView pv= new PageView(this);
//        	pageViews.add(pv);

            pointView iv = new pointView(this);
            pageIndexImgs.add(iv);
            ll.addView(iv);
        }
        pageIndexImgs.get(0).setImageResource(R.drawable.page_now);
//        ll.getChildAt(0).setImageResource(R.drawable.page_now);

    }


    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub
        pageIndexImgs.get(arg0).setImageResource(R.drawable.page_now);
        if (arg0 == 0) {
            pageIndexImgs.get(arg0 + 1).setImageResource(R.drawable.page);
            btnstart.setVisibility(View.VISIBLE);
        } else if (arg0 == pagecount -1) {
            pageIndexImgs.get(arg0 - 1).setImageResource(R.drawable.page);
            //btnstart.setVisibility(View.INVISIBLE);
            btnstart.setVisibility(View.VISIBLE);
        } else {
            pageIndexImgs.get(arg0 - 1).setImageResource(R.drawable.page);
            pageIndexImgs.get(arg0 + 1).setImageResource(R.drawable.page);
            btnstart.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onPageSelected(int arg0) {
        // TODO Auto-generated method stub

    }

    public void onclickstart(View v) {

        Intent intent = new Intent();
        //intent.setClass(this, LoginActivity.class);
        intent.setClass(this, Configuration.class);
        this.startActivity(intent);
        this.finish();

    }
}

class MyPageAdapter extends PagerAdapter {
    final private Integer[] imageIDs = {R.drawable.restaurants, R.drawable.restaurant2, R.drawable.restaurant3, R.drawable.restaurants};
    final String[] texts =new String[]{"EasyOrder App gives customer a better experience to make an order.", "To manage the cooking process for chiefs"
            , "To privide waiter an easy way to serve customers", "Start the App"};
    //String[] titles={"��","��"," "," "};
    final  String[] titles = {"title1", "title2", "title3", "title4"};

    List<PageView> pageViews;
    //LastPageView lastPageView;
    final int pagecount=imageIDs.length;


    public MyPageAdapter(Context context) {

        this.pageViews = new ArrayList<PageView>();
        for (int i = 0; i < pagecount ; i++) {
            PageView pv = new PageView(context, texts[i], imageIDs[i]);
            pageViews.add(pv);

//        	ImageView iv=new ImageView(context);
//        	LayoutParams lp=new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
//        	lp.gravity=Gravity.BOTTOM;
//        	lp.bottomMargin=30;
//        	iv.setLayoutParams(lp);
//        	iv.setScaleType(ScaleType.MATRIX);
//        	iv.setImageResource(R.drawable.page);
//        	pageIndexImgs.add(iv);
        }
        //lastPageView = new LastPageView(context, texts[pagecount - 1], imageIDs[pagecount - 1]);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public int getCount() {
//		return pageViews.size();
        return pagecount;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        if (position < pagecount)
            ((ViewPager) container).removeView(pageViews.get(position));
      /*  else {
            ((ViewPager) container).removeView(lastPageView);
        }*/
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Object instantiateItem(View container, int position) {
//		((ViewPager)container).addView(pageViews.get(position));
        if (position < pagecount) {
            ((ViewPager) container).addView(pageViews.get(position));
            return pageViews.get(position);
        }
        return null;
        /*else {
            ((ViewPager) container).addView(lastPageView);
            return lastPageView;
        }*/
    }

    /**
     * */
    class PageView extends TextView {
        public PageView(Context context, String text, Integer imgID) {
            super(context);
            // TODO Auto-generated constructor stub
            this.setBackgroundResource(imgID);
            LinearLayout.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//	    	lp.gravity=Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL;
//	    	lp.bottomMargin=89;
            this.setLayoutParams(lp);
            this.setTextColor(Color.WHITE);//Color.parseColor("#f0f0f0")
            this.setTextSize(25);
            this.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
            this.setPadding(0, 100, 0, 100);
            this.setText(text);
        }
    }

    /*
    class LastPageView extends RelativeLayout {
        Button btnStart;
        Context ctx;

        public LastPageView(Context context, String text, Integer imgID) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            View inflater = LayoutInflater.from(context).inflate(R.layout.lastpageview, this, true);
            this.getChildAt(0).setBackgroundResource(imgID);
            btnStart = (Button) this.findViewById(R.id.startBtn);
            btnStart.setText(text);

        }

        public void onclickstart(View v) {
            Intent intent = new Intent();
            intent.setClass(this.ctx, LoginActivity.class);
            this.ctx.startActivity(intent);
            ((Activity) this.ctx).finish();
        }
    }*/
}

class pointView extends ImageView {
    public pointView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.BOTTOM;
        lp.bottomMargin = 30;
        lp.rightMargin = 10;
        this.setLayoutParams(lp);
        this.setScaleType(ScaleType.MATRIX);
        this.setImageResource(R.drawable.page);
    }
}