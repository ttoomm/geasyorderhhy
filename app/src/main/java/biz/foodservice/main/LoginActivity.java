package biz.foodservice.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import biz.foodservice.R;
import biz.foodservice.customer.CustomerView;
import biz.foodservice.kitchen.KitchenView;
import biz.foodservice.waiter.WaiterView;

public class LoginActivity extends Activity {

    static final String tag=LoginActivity.class.getCanonicalName();

    private EditText eAccount;
    private EditText ePwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainboard_login);
        eAccount = (EditText) findViewById(R.id.login_account);
        ePwd = (EditText) findViewById(R.id.login_password);

        this.accounts = this.getAccounts();

    }

    public void onclicklogin(View v) {
        boolean isExist = false;

            for (AccountModel account : accounts) {
                if (account.account.equals(eAccount.getText().toString()) &&
                        account.password.equals(ePwd.getText().toString())) {
                    isExist = true;
                    Log.d(tag,  "exist");
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString("key_role", account.role.name());
                    bundle.putString("key_account", account.account);
                    intent.putExtras(bundle);


                    switch (account.role) {
                        case CUSTOMER:
                            intent.setClass(LoginActivity.this, CustomerView.class);
                            Log.d(tag,  "start the customer view");
                            break;
                        case CHIEF:
                            intent.setClass(LoginActivity.this, KitchenView.class);
                            Log.d(tag,  "start kitchen view");
                            break;
                        case WAITER:
                            intent.setClass(LoginActivity.this, WaiterView.class);
                            Log.d(tag,  "start waiter view");
                            break;
                        case ADMINISTRATOR:
                            intent.setClass(LoginActivity.this, Configuration.class);
                            Log.d(tag,  "star admin view");
                            break;
                    }

                    startActivity(intent);
                    this.finish();
                }

            }


        if (!isExist) {

                new AlertDialog.Builder(LoginActivity.this)
                        .setIcon(getResources().getDrawable(R.drawable.error))
                        .setTitle("Alert")
                        .setMessage("Please enter correct account and password!\n")
                        .create().show();

        }
    }


    public void onclickpwd(View v) {
        Toast.makeText(LoginActivity.this, "Please try account--guest, password--123.", Toast.LENGTH_LONG).show();
    }

    List<AccountModel> accounts;

    List<AccountModel> getAccounts() {
        List<AccountModel> accounts = new ArrayList<AccountModel>();
        accounts.add(new AccountModel("waiter1", "123", AccountModel.ROLE.WAITER, 1));
        accounts.add(new AccountModel("waiter2", "123", AccountModel.ROLE.WAITER, 2));
        accounts.add(new AccountModel("waiter3", "123", AccountModel.ROLE.WAITER, 3));
        accounts.add(new AccountModel("chief1", "123", AccountModel.ROLE.CHIEF, 1));
        accounts.add(new AccountModel("chief2", "123", AccountModel.ROLE.CHIEF, 2));
        accounts.add(new AccountModel("customer1", "123", AccountModel.ROLE.CUSTOMER, 3));
        accounts.add(new AccountModel("guest", "123", AccountModel.ROLE.CUSTOMER, 3));
        accounts.add(new AccountModel("customer2", "123", AccountModel.ROLE.CUSTOMER, 4));
        accounts.add(new AccountModel("admin", "123", AccountModel.ROLE.ADMINISTRATOR, 1));
        return accounts;
    }


}
