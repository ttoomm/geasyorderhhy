package biz.foodservice.main;



public class AccountModel {

    static public enum ROLE{
        CUSTOMER, WAITER, CHIEF, ADMINISTRATOR
    }
    public String account;
    public String password;

    private long roleID;

    public ROLE role;

    public AccountModel(String account, String password, ROLE role, long roleID) {
        this.account = account;
        this.password = password;

        this.roleID = roleID;
    }
}
