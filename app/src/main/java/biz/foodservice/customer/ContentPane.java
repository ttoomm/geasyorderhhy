package biz.foodservice.customer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import biz.foodservice.R;
import biz.foodservice.model.Dish;

public class ContentPane extends Fragment {

    CustomerView fa;

    View view;
    GridView gridview;
    List<DishView> dishViews;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /**
         * Inflate the layout for this fragment
         */
        view = inflater.inflate(R.layout.contentpane, container, false);
        if (fa.dishes!=null) {
            gridview = (GridView) view.findViewById(R.id.gridview);
            gridview.setHorizontalScrollBarEnabled(true);
            gridview.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

            gridview.setAdapter(new ImageAdapter(view.getContext(), fa.dishes, this.fa, this));

            this.dishViews = new ArrayList<DishView>();
        }

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.fa = (CustomerView) activity;
        this.fa.contentPane = this;
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
    }

    /**
     * ****************class ImageAdapter*********************
     */
    static public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        CustomerView ma;
        ContentPane cp;

        List<Dish> dishes;

        final BitmapFactory.Options options;

        // Constructor
        public ImageAdapter(Context c, List<Dish> dishes, CustomerView ma, ContentPane cp) {
            this.ma = ma;
            this.cp = cp;

            mContext = c;
            this.dishes = dishes;
            this.options = new BitmapFactory.Options();
            options.inSampleSize = 1;

//			mThumbIds = new Integer[10];
//			for (int i = 0; i < mThumbIds.length; i++) {
//				mThumbIds[i] = R.drawable.ic_launcher;
//			}
        }

        @Override
        public int getCount() {
            //return mThumbIds.length;
            return this.dishes.size();
            //int i=16;
            //Log.d("aa", String.format("dish id: %d, name: %s, icon name: %s", this.dishes[i].dish.id, this.dishes[i].dish.name, this.dishes[i].dish.icon));
            //return 16;
        }

        //get an Item in gridview
        @Override
        public Object getItem(int position) {
            Log.d("aa", "get the postion: " + position);
            return this.dishes.get(position);
            //return null;
        }

        @Override
        public long getItemId(int position) {
            return this.dishes.get(position).id;
            //return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Dish di = this.dishes.get(position);
            DishView dv = null;
            for (int i = 0; i < this.cp.dishViews.size(); i++) {
                dv = this.cp.dishViews.get(i);
                if (dv.dish.id == di.id) {
                    return dv;
                }
            }
            dv = new DishView(this.mContext, null, di, this.ma);
            this.cp.dishViews.add(dv);
            return dv;
        }

    }// class ImageAdapter
}
