package biz.foodservice.customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import biz.foodservice.R;
import biz.foodservice.model.Order;



public class PayPane extends Fragment {

    public static final String tag=PayPane.class.getCanonicalName();


    ListView orderlistView;
    Button btnPay;
    Button btnExit;
//	TextView tvTableid;

    PayOrderList orderList;
    //Database dbTransaction;

    CustomerView fa;
    View view;

//	public PayPane() throws FileNotFoundException, IOException, ClassNotFoundException{
//		super();
//		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//	    StrictMode.setThreadPolicy(policy);
//	}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        view = inflater.inflate(R.layout.pay_main, container, false);
        orderlistView = (ListView) view.findViewById(R.id.orderlist);
        btnPay = (Button) view.findViewById(R.id.btnPay);
        btnExit = (Button) view.findViewById(R.id.btnExit);
//		tvTableid = (TextView) view.findViewById(R.id.tableid);
        Log.d(tag, "customer id:" + fa.customer);
//		this.orderList= new PayOrderList(fa,3L);
        this.orderList = new PayOrderList(fa, fa.customer);

            refreshOrderList();
            //orderlistView.setAdapter(this.orderList);


        btnExit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d(tag,  "Back click");

                fa.payLayout.setVisibility(View.GONE);
                onPause();
            }
        });

        btnPay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d(tag,  "pay click");
                boolean ischeck = false;
                double totalItemsPrice = 0;
                double taxRate = 0.085;
                Log.d(tag,  "1 customer id:" + fa.customer);
//				for(OrderModel od:orderList.orders){
                for (int i = orderList.orders.size() - 1; i > -1; i--) {
                    Order od = orderList.orders.get(i);
                    //PayOrderView ov=od.getView(fa);
                    PayOrderView ov = null;
                    if (ov.isChecked()) {
                        totalItemsPrice += ov.itemsPrice;
                        ischeck = true;

                        // update the order to be payed.
                        od.paid = true;
                            /*
							try {
								dbTransaction.saveDocument(od.getDoc(), od.id);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							*/
                        Log.d(tag,  String.format(
                                "update status to be processed,  order %s, table: %d",
                                od.id, od.table));
//							refreshOrderList();
                        orderList.orders.remove(od);

                    }
                }
                orderList.notifyDataSetChanged();

                new AlertDialog.Builder(fa)
                        .setIcon(getResources().getDrawable(R.drawable.easyorder3))
                        .setTitle("Thank you!")
                        .setMessage(ischeck ? String.format("You Pay:\n Items: $%.2f       Tax: $%.2f 		Total: $%.2f ",
                                totalItemsPrice, totalItemsPrice * taxRate, totalItemsPrice + totalItemsPrice * taxRate) :
                                "Please select orders to pay, Thank you!")
                        .create().show();


            }
        });


        return view;
//		return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        this.fa = (CustomerView) activity;
        this.fa.payPane = this;
    }

    public void refreshOrderList() {
		/*
		dbTransaction = null;
		try {
		Session se=new Session(ConfigUtil.getHost(), ConfigUtil.getPort());
		dbTransaction=se.getDatabase(ConfigUtil.DBNAME_TRANSACTION);
			if (dbTransaction == null) {
				dbTransaction = se.createDatabase(ConfigUtil.DBNAME_TRANSACTION);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Log.d(DebugTool.LOG_TAG, LOG_ARROW+"start pulling the tasks, "+String.format("couchdb://%s:%d/%s", 
				ConfigUtil.getHost(), ConfigUtil.getPort(), ConfigUtil.DBNAME_TRANSACTION));
		try {
			Log.e(LOG_TAG,LOG_ARROW+"pullorders begin");
			this.orderList.pullOrders(dbTransaction);
			Log.e(LOG_TAG,LOG_ARROW+"pullorders ok");
		} catch (IOException e) {
			Log.e("aa",LOG_ARROW+"pullorders error");
			e.printStackTrace();
		}
		*/
        orderList.notifyDataSetChanged();
//		orderlistView.invalidateViews();
    }

    @Override
    public void onDestroyView() {
        // TODO Auto-generated method stub
        super.onDestroyView();
        this.orderList = null;
        this.orderlistView = null;
        Log.d(tag, "onDestroyView");
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
//		orderList.notifyDataSetChanged();
        refreshOrderList();
        Log.d(tag,  "onResume");
    }


}
