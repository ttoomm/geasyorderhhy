package biz.foodservice.customer;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import biz.foodservice.R;
import biz.foodservice.customer.ContentPane.ImageAdapter;
import biz.foodservice.model.Catagory;
import biz.foodservice.model.Dish;

public class SidePane extends ListFragment {
    private static final String tag=SidePane.class.getCanonicalName();


    CustomerView fa;

    Catagory[] cats;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (fa.cats!=null) {
            String[] catNames = new String[fa.cats.size()];
            for (int i = 0; i < catNames.length; i++) {
                catNames[i] = fa.cats.get(i).name;
            }

            List<Long> allDishId = new ArrayList<Long>();
            for (Dish dish : this.fa.dishes) {
                allDishId.add(dish.id);
            }
            Catagory all = new Catagory(-1, "ALL", "", "", allDishId);
            fa.cats.add(0, all);

            cats = fa.cats.toArray(new Catagory[0]);


            ArrayAdapter<Catagory> adapter = new ArrayAdapter<Catagory>(getActivity(),
                    R.layout.sidepane_list_item, cats);

            setListAdapter(adapter);

        }

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

//	    Toast t=Toast.makeText(this.getActivity(), "I am clicked: "+position+", the catagory id: "+this.cats[position].id, 3);
//	    t.show();

        List<Dish> dl = new ArrayList<Dish>();
        for (int i = 0; i < this.fa.dishes.size(); i++) {
            Dish inf = this.fa.dishes.get(i);
            if (this.cats[position].dishes.contains(inf.id)) {
                dl.add(inf);
            }
        }


        this.fa.contentPane.gridview.setAdapter(new ImageAdapter(this.fa.contentPane.view.getContext(), dl, this.fa, this.fa.contentPane));

        this.fa.contentPane.view.invalidate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        /**
         * Inflate the layout for this fragment
         */

        return inflater.inflate(
                R.layout.sidepane, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(tag, "class: " + activity.getClass().getCanonicalName());
        this.fa = (CustomerView) activity;
        fa.sidepane = this;

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
    }


}
