package biz.foodservice.customer;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import biz.foodservice.R;
import biz.foodservice.model.Dish;
import biz.foodservice.model.Order;


public class PayOrderView extends LinearLayout {
    public static final String LOG_TAG = PayOrderView.class.getCanonicalName();


    public Order orderModel;
    boolean mchecked = false;
    boolean mcheckable = true;
    boolean isClickable = true;
    Context ctx;
    double itemsPrice = 0;
    double taxRate = 0.085;

    ViewHolder viewholder;

    class ViewHolder {
        CheckedTextView orderid;
        HorizontalScrollView hsv;
        LinearLayout orderPane;
        TextView sum;
    }

    List<PayDishView> dishViews;

    //the resource
    List<Dish> dishes;

    public PayOrderView(Context context, Order orderModel, List<Dish> dishes) {
        this(context, orderModel, false);


    }

    public PayOrderView(Context context, Order orderModel, boolean small) {
        super(context);
        this.orderModel = orderModel;
        this.ctx = context;

        this.viewholder = new ViewHolder();
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.pay_orderview, this, true);
//	    this.setOrientation(LinearLayout.HORIZONTAL);
        viewholder.orderid = (CheckedTextView) this.findViewById(R.id.orderid);
        viewholder.hsv = (HorizontalScrollView) this.findViewById(R.id.hsv_order);
        viewholder.orderPane = (LinearLayout) this.findViewById(R.id.orderPane);
        viewholder.sum = (TextView) this.findViewById(R.id.sum);

        LinearLayout ll = (LinearLayout) this.getChildAt(0);
        if (this.orderModel.timestamp != 0) {
            viewholder.orderid.setText(String.format("Table#: %d      OrderID: %s      OrderTime: %1$tm/%1$td/%1$tY %1$tH:%1$tM",
                    this.orderModel.table, this.orderModel.id, this.orderModel.timestamp));
            //OrderTime:%1$tm/%1$td/%1$tY %1$tH:%1$tM:%1$tS
        } else {
            viewholder.orderid.setText(String.format("Table#: %d       OrderID: %s ",
                    this.orderModel.table, this.orderModel.id));
        }
        this.dishViews = new ArrayList<PayDishView>();
        for (long dish : this.orderModel.dishes.keySet()) {
            PayDishView tView = new PayDishView(context, dish, this.orderModel.dishes.get(dish), true, this.dishes);
            this.itemsPrice += tView.price * this.orderModel.dishes.get(dish) / 100;
            dishViews.add(tView);
            viewholder.orderPane.addView(tView);
        }

        viewholder.sum.setText(String.format("Items: $%.2f       Tax: $%.2f 		Total: $%.2f",
                this.itemsPrice, this.itemsPrice * this.taxRate, this.itemsPrice + this.itemsPrice * this.taxRate));

        viewholder.orderPane.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                toggle();
            }
        });

        viewholder.orderid.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                toggle();
            }
        });

        viewholder.sum.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                toggle();
            }
        });
    }

    public void setChecked(boolean checked) {
        Log.d(LOG_TAG,  "task table: " + orderModel.table + " setchecked():" + checked);
        this.mchecked = checked;
        if (isChecked()) {
            viewholder.hsv.setBackgroundColor(Color.parseColor("#fd9801"));
        } else {
            viewholder.hsv.setBackgroundColor(Color.parseColor("#e3d6cc"));
        }
    }


    public boolean isChecked() {
        return this.mchecked;
    }


    public void toggle() {
        setChecked(!this.mchecked);
    }


}
