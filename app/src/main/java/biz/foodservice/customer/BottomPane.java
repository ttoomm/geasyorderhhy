package biz.foodservice.customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;

import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import biz.foodservice.R;
import biz.foodservice.model.Dish;
import biz.foodservice.model.Order;

import biz.foodservice.tools.NetworkTools;

public class BottomPane extends Fragment implements OnClickListener, OnCancelListener, OnDismissListener {

    CustomerView mainAct;
    TextView sum;

    LinearLayout ll;

    List<Dish> dishes;

    List<DishView> dvs;
    HorizontalScrollView hsv;//

    Button btnSubmit;

    //EditText vNumCustomer;

    int idTable;
    //TextView vTableId;
    Spinner vTableId;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        /**
         * Inflate the layout for this fragment
         */
        View v = inflater.inflate(
                R.layout.bottompane, container, false);

        sum = (TextView) v.findViewById(R.id.sum);

        hsv = (HorizontalScrollView) v.findViewById(R.id.horizontalScroll);//

        ll = (LinearLayout) v.findViewById(R.id.orderPane);
        this.dishes = new ArrayList<Dish>();
        this.dvs = new ArrayList<DishView>();
        this.setSum();
        this.btnSubmit = (Button) v.findViewById(R.id.submit_order);
        this.btnSubmit.setOnClickListener(this);
        this.btnSubmit.setEnabled(false);

        this.vTableId = (Spinner) v.findViewById(R.id.idTable);
        //this.vNumCustomer = (EditText) v.findViewById(R.id.numCustomer);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.mainAct,
                R.array.table_array, android.R.layout.preference_category);
        this.vTableId.setAdapter(adapter);
        TableSelection tbs = new TableSelection(this);
        this.vTableId.setOnItemSelectedListener(tbs);

        //vTableId.setKeyListener(this);
        //this.vTableId.addTextChangedListener(this);
        //this.vTableId.setAdapter(this);

        return v;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mainAct = (CustomerView) activity;
        this.mainAct.bottomPane = this;

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
    }

    void addDish(Dish dish) {
        if (this.dishes.contains(dish)) {
            return;
        }


        DishView dv = new DishView(this.getActivity().getApplicationContext(), null, dish, this.mainAct);

        this.dishes.add(dish);
        this.dvs.add(dv);

        dv.ib.setClickable(true);
        dv.ib.setOnClickListener(this);
        dv.ib.setLongClickable(false);
        this.setSum();

        this.ll.addView(dv);

        //scroll to right
        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                hsv.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
    }

    Order getOrderModel() {
        int idTable = Integer.parseInt((String) this.vTableId.getSelectedItem());
        Order order = new Order(idTable, mainAct.customer);
        for (Dish dish : this.dishes) {
            order.addDish(dish.id, 1);
        }
        return order;
    }

    final static String urlPathForPostOrder="/rest/transaction/order/post";


    private boolean submitOrder()  {
        long idTable=Integer.parseInt(this.vTableId.getSelectedItem().toString());

        Order om = this.getOrderModel();

        Log.d(tag, "before placed the order");
        String host=mainAct.host;
        int port=mainAct.port;
        String url = String.format("http://%s:%d%s", host, port, urlPathForPostOrder);
        String orderIdReturn ="";
        long orderId=-1;
        try {
            orderIdReturn= NetworkTools.postJsonData(url, om);
            orderId= Long.parseLong(orderIdReturn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(orderId<0){
            new AlertDialog.Builder(this.mainAct)
                    .setIcon(getResources().getDrawable(R.drawable.easyorder3))
                    .setTitle("Sorry")
                    .setMessage("sorry, your order is not placed, please try it again.")
                    .create().show();
            return false;
        }


        this.dlgPopup = null;

        Log.d(tag, "disable the submit btn");


        //this.getView().invalidate();
        Log.d(tag, "refresh");

        // this.vTableId.setText("");
        this.vTableId.setSelection(0);
        this.btnSubmit.setEnabled(false);
        Log.d(tag, "set the submit btn to false");

        //notify
//		this.sum.setText("your order has been placed");
        new AlertDialog.Builder(this.mainAct)
                .setIcon(getResources().getDrawable(R.drawable.easyorder3))
                .setTitle("Thank you!")
                .setMessage("Your order has been placed, order id: "+orderIdReturn)
                .create().show();

        for (DishView dv : this.dvs) {
            for (DishView dvb : this.mainAct.contentPane.dishViews) {
                if (dvb.dish.id == dv.dish.id) {
                    dvb.isOrdered = false;
                    dvb.setPic();
                }
            }
        }
        this.dishes.clear();
        this.dvs.clear();
        this.ll.removeAllViews();

        Log.d(tag, "finish place order");
        return true;
    }

    Set<Dish.Allergen> getAllergens(Order om) {
        Set<Dish.Allergen> allergens = new HashSet<Dish.Allergen>();
        for (long idDish : om.dishes.keySet()) {
            Dish dm = Dish.getDishModel(dishes, idDish);
            for (Dish.Allergen a : dm.allergens.keySet()) {
                if (dm.allergens.get(a))
                    allergens.add(a);
            }
        }
        return allergens;

    }

    Map<Dish.Nutrition, Float> getNutritions(Order om) {
        Map<Dish.Nutrition, Float> m = new HashMap<Dish.Nutrition, Float>();
        for (Dish.Nutrition n : Dish.Nutrition.values()) {
            m.put(n, 0f);
        }
        for (long idDish : om.dishes.keySet()) {
            Dish dm = Dish.getDishModel(dishes, idDish);
            for (Dish.Nutrition n : dm.nutritions.keySet()) {
                float f = m.get(n);
                f += dm.nutritions.get(n);
                m.put(n, f);
            }
        }
        return m;

    }

    Dialog dlgPopup;

    private void showHealthInfo() throws IOException {

        this.dlgPopup = new Dialog(this.mainAct);
        dlgPopup.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout , null));
        dlgPopup.setOnCancelListener(this);
        dlgPopup.setOnDismissListener(this);
        //dlgPopup.setContentView(new HealthInfo(this));
        dlgPopup.show();

    }

    static final String tag = BottomPane.class.getCanonicalName();

    @Override
    public void onClick(View v) {
        if (v == this.btnSubmit) {
            Log.d(tag, "about to submit");


            this.submitOrder();
            //this.showHealthInfo();


            return;
        }

        Log.d(tag, "remove clicked");
        if (!(v instanceof ImageButton)) {
            return;
        }

        ImageButton _ib = (ImageButton) v;
        int index = -1;
        for (int i = 0; i < this.dvs.size(); i++) {
            ImageButton ib = this.dvs.get(i).ib;
            if (ib == _ib) {
                index = i;
            }
        }
        Log.d(tag, "remove dishhh: " + index);
        DishView dv = (DishView) this.dvs.get(index);

        this.ll.removeView(dv);
        this.dvs.remove(index);
        this.dishes.remove(index);

        for (DishView dvb : this.mainAct.contentPane.dishViews) {
            if (dvb.dish.id == dv.dish.id) {
                dvb.isOrdered = false;
                dvb.setPic();
            }
        }

        this.setSum();
        if (this.dishes.size() > 0 && this.vTableId.getSelectedItemPosition() != 0) {
            this.btnSubmit.setEnabled(true);
        } else {
            this.btnSubmit.setEnabled(false);
        }
    }

    float getTotal() {
        float total = 0;
        for (Dish info : this.dishes) {
            total += info.price;
        }
        return total;
    }

    void setSum() {
        sum.setText(String.format("total: $%.2f   items: %d",
                0.01 * this.getTotal(), this.dishes.size()));//"total: $%.2f\nitems: %d"
    }

    static class TableSelection implements OnItemSelectedListener {
        BottomPane bp;

        TableSelection(BottomPane bp) {
            this.bp = bp;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            if (0 == position) {
                this.bp.btnSubmit.setEnabled(false);
            } else {
                if (this.bp.dishes.size() > 0)
                    this.bp.btnSubmit.setEnabled(true);
            }


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            this.bp.btnSubmit.setEnabled(false);

        }

    }

/* temporary disable the health information
    static class HealthInfo extends WebView {
        BottomPane bp;
        Set<Dish.Allergen> allergens;
        Map<Dish.Nutrition, Float> ns;

        String btnSubmit = "<input type=\"button\" value=\"submit\" onclick=\"window.JSInterface.placeOrder();\" > I am ok with the intake of nutrition / calories</input>";
        String btnCancel = "<input type=\"button\" value=\"cancel\" onclick=\"window.JSInterface.changeOrder();\" > Modify order</input>";


        public HealthInfo(BottomPane bp) {
            super(bp.mainAct);
            this.bp = bp;
            this.allergens = bp.getAllergens(bp.getOrderModel());
            this.ns = bp.getNutritions(bp.getOrderModel());

            JavaScriptInterface jsInterface = new JavaScriptInterface(bp);
            this.getSettings().setJavaScriptEnabled(true);
            this.addJavascriptInterface(jsInterface, "JSInterface");


            String htmlNutritions = String.format("<table border=\"1\"><tr><th>Nutritions</th><th>units</th><th>recommennded unit</th></tr>");
            for (Dish.Nutrition n : this.ns.keySet()) {
                float q = this.ns.get(n);
                String txtNumCustomer = this.bp.vNumCustomer.getText().toString();
                int nCustomer = txtNumCustomer.length() == 0 ? 1 : Integer.parseInt(txtNumCustomer);
                nCustomer = nCustomer == 0 ? 1 : nCustomer;

                String style = "";
                if (n.recommended > 0) {
                    if (n.recommended < q / nCustomer) {
                        style = "yellow";
                    } else if (n.recommended > q / nCustomer) {
                        style = "green";
                    }
                }


                htmlNutritions += String.format("<tr bgcolor=\"%s\"><td>%s: </td><td>%4.2f</td><td>%s</td></tr>", style, n, q, n.recommended > 0 ? String.format("%4.2f", n.recommended) : "Not available");
            }
            htmlNutritions += String.format("</table>");

            String htmlAllergens = String.format("<ul><b>Allergens</b>");
            for (Dish.Allergen a : this.allergens) {
                htmlAllergens += String.format("<li>%s</li>", a);
            }


            htmlAllergens += String.format("</ul>");

            String content = "";
            String htmlDetail = String.format("<html><body>"
                    + "<table><tr><td>%s</td><td>%s</td></tr></table><p bgcolor=\"#CCCC00\">%s</p><p>%s</p><p>%s</p>"
                    + "</body></html>", htmlNutritions, htmlAllergens, content, this.btnSubmit, this.btnCancel);


            this.loadData(htmlDetail, "text/html", null);

        }


    }
*/

    @Override
    public void onCancel(DialogInterface dialog) {

        Log.d(tag, "cancel");
        this.dlgPopup.setOnDismissListener(null);
        this.dlgPopup = null;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Log.d(tag, "dismiss");
        this.dlgPopup = null;

            this.submitOrder();



    }


}
