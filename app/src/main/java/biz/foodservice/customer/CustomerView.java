package biz.foodservice.customer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import biz.foodservice.R;
import biz.foodservice.main.Configuration;
import biz.foodservice.model.Catagory;
import biz.foodservice.model.Dish;
import biz.foodservice.model.ResourceInfo;


public class CustomerView extends Activity {

    SidePane sidepane;
    ContentPane contentPane;
    BottomPane bottomPane;

    PayPane payPane;
    LinearLayout payLayout;

    String account="guest";

    public static final String AUTHORITY = "biz.foodservice";


    /**
     * *******config info   *********
     */

    String host;
    int port;


	/*
     * **init Data*********/
    //


    List<Dish> dishes;
    List<Catagory> cats;

    final static String tag=CustomerView.class.getCanonicalName();

    long customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        Log.d(tag, "starting the onCreate");

        this.customers = getCustomers();
        /*
        Bundle bundle = new Bundle();
        bundle = this.getIntent().getExtras();
*/

        SharedPreferences settings=this.getSharedPreferences(Configuration.preferenceName, 0);
        this.host=settings.getString("server", "");
        this.port=settings.getInt("port", -1);

        if(this.host.length()<1 || this.port<0){
            Toast.makeText(this, "the server setting is not valid, set up the server at the first page", Toast.LENGTH_LONG);
            Intent intent=new Intent(this, Configuration.class);
            this.startActivity(intent);
        }


        //debug mode
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        //init data
        String localStorageDir=this.getFilesDir().getAbsolutePath();
        Log.d(tag, "get local storage directory: " + localStorageDir);
        try {
            ResourceInfo ri=new ResourceInfo(localStorageDir);
            ri.loadFromLocal();
            this.dishes=ri.getDishesInfo();
            this.cats= ri.getCatagoryInfo();
        } catch (ClassNotFoundException e) {
            String msg="fail to load resource from local storage, cause: "+e.getMessage();
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            Log.e(tag, msg);
        } catch (IOException e) {
            String msg="fail to load resource from local storage, cause: "+e.getMessage();
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            Log.e(tag, msg);
        }

        this.setContentView(R.layout.customer_view);
        payLayout = (LinearLayout) this.findViewById(R.id.paylayout);
        Log.d(tag, "finish the onCreate");

    }








    List<CustomerModel> customers;

    List<CustomerModel> getCustomers() {
        List<CustomerModel> customers = new ArrayList<CustomerModel>();
        customers.add(new CustomerModel(1, "Admin"));
        customers.add(new CustomerModel(2, "Guest"));
        customers.add(new CustomerModel(3, "Sunny"));
        customers.add(new CustomerModel(4, "Lucy"));
        return customers;
    }







    /**
     * init end
     * */


    /**
     * ActionBar**
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.customer, menu);
        MenuItem accountItem = menu.findItem(R.id.action_account);
        accountItem.setTitle("HI!  " + this.account + "  ");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_pay) {
            payLayout.setVisibility(View.VISIBLE);
            payPane.onResume();
            return true;
        } else if (id == R.id.action_facebook) {
            Uri uri = Uri.parse("https://www.facebook.com/BoilingPointUSA");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
