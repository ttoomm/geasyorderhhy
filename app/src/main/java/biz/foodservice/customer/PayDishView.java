package biz.foodservice.customer;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import biz.foodservice.model.Dish;

public class PayDishView extends TextView {

    public static final String LOG_ARROW = PayDishView.class.getCanonicalName();

    long dish;
    long dishnum;
    long price;

    final BitmapFactory.Options options;

    List<Dish> dishes;


    public PayDishView(Context context, long dish, long dishnum, boolean small, List<Dish> dishes) {
        super(context);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(250, LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 1, 2, 1);
        setLayoutParams(params);
        //set options
        this.options = new BitmapFactory.Options();
        this.setGravity(Gravity.BOTTOM);
        if (small) {
            options.inSampleSize = 2;
            this.setTextColor(Color.CYAN);
        } else {
            options.inSampleSize = 1;
            this.setTextColor(Color.parseColor("#514f98"));
        }

        this.setTextSize(10.0f);
//	    this.setPadding(0, 2, 2, 2);
//		android:layout_marginRight="2dp"


        //init tm
        this.dish = dish;
        this.dishnum = dishnum;
        this.dishes = dishes;
        //init view
        for (Dish dm : dishes) {
            if (this.dish == dm.id) {
//				this.setText(dm.name);
                this.setText(String.format("%s -- [%s]", dm.name, this.dishnum));
                this.price = dm.price;
                Bitmap bm = BitmapFactory.decodeFile(dm.icon, this.options);
                BitmapDrawable sd = new BitmapDrawable(bm);
//				this.setBackground(sd);
//				BitmapDrawable drawable=new BitmapDrawable(getResources(),bm);
//				BitmapDrawable drawable=writeOnDrawable(R.drawable.badge, "1");
//				this.setBackground(drawable);

                this.setCompoundDrawablesWithIntrinsicBounds(null, sd, null, null);
                this.setCompoundDrawablePadding(10);
            }
        }

//		Log.i(DebugTool.LOG_TAG,LOG_ARROW+"construction isChecked():"+isChecked());
    }

//	public BitmapDrawable writeOnDrawable(int drawableId, String text){
//
//        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);
//
//        Paint paint = new Paint(); 
//        paint.setStyle(Style.FILL);  
//        paint.setColor(Color.BLACK); 
//        paint.setTextSize(20); 
//
//        Canvas canvas = new Canvas(bm);
//        canvas.drawText(text, 0, bm.getHeight()/2, paint);
//
//        return new BitmapDrawable(bm);
//    }

//	@Override
//	protected void onDraw(Canvas canvas) {
//		// TODO Auto-generated method stub
//		//set status icon
////		this.setCheckMarkDrawable(this.isChecked()?R.drawable.icon_choice:R.drawable.gv_selected_no);
////		Log.i(DebugTool.LOG_TAG,LOG_ARROW+"ondraw isChecked():"+isChecked());
//		super.onDraw(canvas);
//	}

//	public void setStatus(DeliverTaskModel.Status status){
//		if(status==DeliverTaskModel.Status.DELIVERING){
//			Drawable sd=this.getResources().getDrawable(R.drawable.icon_delivering);
//			
//			//set by background
////			Drawable a=this.getBackground();
////			LayerDrawable ld=new LayerDrawable(new Drawable[]{a, sd});
////			this.setBackground(ld);	
//			
//			//set by CompoundDrawables
//			Drawable[] drawables=this.getCompoundDrawables();
//			if (drawables != null) {
//		            Drawable drawableTop = drawables[1];
//		            if(drawableTop!=null){
//			            Log.i(DebugTool.LOG_TAG,LOG_ARROW+"drawabletop:"+drawableTop);
//			            if (drawableTop != null) {
//				           // LayerDrawable ld1=new LayerDrawable(new Drawable[]{drawableTop, sd});
//			            	LayerDrawable ld1=new LayerDrawable(new Drawable[]{sd,drawableTop});
//							this.setCompoundDrawablesWithIntrinsicBounds(null, ld1, null,null);
//							this.setCompoundDrawablePadding(0);
//			            }
//		            }
//			}
//		}
//		//else if(status==DeliverTaskModel.Status.WAITING || status==DeliverTaskModel.Status.READY){
//		else if(status==DeliverTaskModel.Status.AVAILABLE){
//			for(CookingDishInfoModel dm: waiterActivity.dishInfoModels){
//				if(this.dish==dm.id){
////					this.setText(String.format("%s -- [%s]", dm.name, this.dishnum));
//					Bitmap bm = BitmapFactory.decodeFile(dm.icon, this.options);
//					BitmapDrawable sd=new BitmapDrawable(bm);
////					this.setBackground(sd);
//					this.setCompoundDrawablesWithIntrinsicBounds(null,  sd,null,   null);
//					this.setCompoundDrawablePadding(0);
//				}
//			}
//		}
//	}

}
