package biz.foodservice.customer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import biz.foodservice.model.Order;


public class PayOrderList extends BaseAdapter {
    public static final String LOG_TAG = PayOrderList.class.getCanonicalName();


    List<Order> orders;
    Context ctx;
    //	Long table;
    Long customer;

    public PayOrderList(Context ctx, Long customer) {

        this.ctx = ctx;
//		this.table=table;
        this.customer = customer;
        orders = new ArrayList<Order>();
//		OrderModel om=new OrderModel(1L, 3L);
//		orders.add(om);
//		OrderModel om1=new OrderModel(2L, 3L);
//		orders.add(om1);
        //pullOrders();
    }

    /*
    public List<OrderModel> pullOrders(Database db)throws IOException {

        ViewResults qryRst=null;
        qryRst=db.adhoc(this.getOrderbycustomer());
        List<Document> cmOrders=qryRst.getResults();
        this.orders.clear();
        Log.d(LOG_TAG, LOG_ARROW+"total unhandled order: "+ cmOrders.size());
//		List<OrderModel> orders=new ArrayList<OrderModel>();
        for(Document doc: cmOrders){


            JSONObject json=doc.getJSONObject("key");
            OrderModel om=new OrderModel(json);

            Log.d(LOG_TAG, LOG_ARROW+String.format("id: %s, table: %d, dish number: %d", om.id, om.table, om.dishes.keySet().size()));
            this.orders.add(om);
        }

        return orders;


    }
    */
    String getOrderbycustomer() {
        return String.format("function(doc) { if( doc.type && doc.type =='%s' && !(doc.payed) " +
                "&& (doc.customer =='%s')) emit(doc);}", Order.TYPE, this.customer);
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return orders.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //return this.orders.get(position).getView(this.ctx);
        return null;
//		TextView view=new TextView(this.ctx);
    }

}
