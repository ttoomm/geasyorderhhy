package biz.foodservice.customer;

import android.os.AsyncTask;

import java.io.IOException;

import biz.foodservice.model.Order;
import biz.foodservice.tools.NetworkTools;

/**
 * Created by hx on 4/22/15.
 */
public class OrderHelper extends AsyncTask<Object, Long, Long> {

    BottomPane bp;
    OrderCmd cmd;

    static public enum OrderCmd {
        Check_Order, Put_Order
    }


    OrderHelper(OrderCmd cmd, BottomPane bp) {

        this.bp = bp;
        this.cmd = cmd;
    }


    @Override
    protected void onPostExecute(Long aLong) {
        if (this.cmd == OrderCmd.Check_Order) {

        } else if (this.cmd == OrderCmd.Put_Order) {

        }
    }

    @Override
    protected Long doInBackground(Object... objs) {
        if (this.cmd == OrderCmd.Check_Order) {

        } else if (this.cmd == OrderCmd.Put_Order) {
            if (objs.length != 2)
                return 0L;
            String url = (String) objs[0];
            Order order = (Order) objs[1];
            String s = "";
            try {
                s = NetworkTools.postJsonData(url, order);
            } catch (IOException e) {
                e.printStackTrace();
                return 0L;
            }
            return Long.parseLong(s);

        }
        return 0L;
    }
}
