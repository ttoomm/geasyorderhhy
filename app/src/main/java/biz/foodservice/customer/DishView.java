package biz.foodservice.customer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;

import biz.foodservice.R;
import biz.foodservice.model.Dish;

/**
 * *****
 * Each Item in TaskPane
 * *****************
 */
public class DishView extends LinearLayout implements OnClickListener, OnLongClickListener {
    CustomerView mainAct;
    final BitmapFactory.Options options;
    Context ctx;
    public Dish dish;
    ImageButton ib;
    boolean isOrdered = false;

    public DishView(Context context, AttributeSet attrs, Dish dish, CustomerView ma) {
        super(context, attrs);

        this.ctx = context;
//		setOrientation(LinearLayout.HORIZONTAL);
//		setGravity(Gravity.CENTER_VERTICAL);

        // requests the decoder to subsample the original image, returning a smaller image to save memory.
        this.options = new BitmapFactory.Options();
        options.inSampleSize = 3;

        this.dish = dish;

        //load layout
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.dish, this, true);

        LinearLayout ll = (LinearLayout) this.getChildAt(0);
        ib = (ImageButton) ll.getChildAt(0);
        //CheckBox cb=(CheckBox) ll.getChildAt(1);
        TextView cb = (TextView) ll.getChildAt(1);

        this.setPic(); //make sure: "ib" already init

        this.mainAct = ma;
        ib.setOnClickListener(this);
        ib.setOnLongClickListener(this);

        cb.setTextSize(10.0f);
        cb.setText(String.format("%s\n $%.2f", dish.name, 0.01 * dish.price));
    }

    //set is_ordered icon mark
    public void setPic() {
        Bitmap bm = BitmapFactory.decodeFile(this.dish.icon, this.options);
        ib.setBackground(new BitmapDrawable(bm));

        if (this.isOrdered) {
            ib.setImageResource(R.drawable.is_ordered);
        } else {
            ib.setImageDrawable(null);
        }
    }

    static final String tag=DishView.class.getCanonicalName();
    //dish is selected
    @Override
    public void onClick(View v) {
        Log.d(tag, "dishdd is selected(touched), " + this.dish.name);
        DishView dv = null;
        this.isOrdered = true;
        this.mainAct.bottomPane.addDish(this.dish);
        //set is_ordered icon mark
        this.setPic();
        //this.mainAct.bottomPane.getView().invalidate();
    }

    /**
     * **
     * show detail popup window******
     */
    @Override
    public boolean onLongClick(View v) {
        Log.d(tag, "longclick");
        Detail detail = new Detail(this.ctx, this.dish);
        this.loadPhoto(detail, 150, 200);
        return true;
    }

    private void loadPhoto(Detail detail, int width, int height) {
        Dialog settingsDialog = new Dialog(this.ctx);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout , null));
        settingsDialog.setContentView(detail);
        settingsDialog.show();
    }

    public static class Detail extends WebView {
        public Detail(Context context, Dish dishInfo) {
            super(context);

            Bitmap bitmap = BitmapFactory.decodeFile(dishInfo.pic);
            //this.setBackground(new BitmapDrawable(bm));

//			Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.my_drawable);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            String base64Image = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
            String imgHtml = String.format("<img src=\"data:image/jpg;base64," + base64Image +
                    "\" style=\"height: %d; width: %d; float: left\";/>", 420, 380);

            String content = String.format("<p>%s</p>", dishInfo.desc == null ? "" : dishInfo.desc);
            String htmlNutritions="";
            if(dishInfo.nutritions!=null){
                htmlNutritions= String.format("<table border=\"1\"><tr><th>Nutritions</th><th>units</th></tr>");

                for (Dish.Nutrition n : dishInfo.nutritions.keySet()) {

                    htmlNutritions += String.format("<tr><td>%s: </td><td>%2.2f</td></tr>", n, dishInfo.nutritions.get(n));
                }
                htmlNutritions += String.format("</table>");
            }
            String htmlAllergens="";
            if(dishInfo.allergens!=null) {
                htmlAllergens = String.format("<ul><b>Allergens</b>");
                for (Dish.Allergen a : dishInfo.allergens.keySet()) {
                    if (dishInfo.allergens.get(a))
                        htmlAllergens += String.format("<li>%s</li>", a);
                }


                htmlAllergens += String.format("</ul>");
            }



            String htmlDetail = String.format("<html><body>"
                    + "<table><tr><td>%s</td><td>%s%s</td></tr></table><p bgcolor=\"#CCCC00\">%s</p>"
                    + "</body></html>", imgHtml, htmlNutritions, htmlAllergens, content);


            this.loadData(htmlDetail, "text/html", null);


        }
    }


}
